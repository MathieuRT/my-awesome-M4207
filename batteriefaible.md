```c

#include <LBattery.h>

char buff[256];


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
   pinMode(13, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
 if ( LBattery.level() >20){         //si la batterie est superieur a 20% le voyant reste allumer 
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(250);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(250);              // wait for a second
 }
 else if ( LBattery.level() <20){si la batterie est inferieur a 20% le voyant clignote 
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(250);              // wait for a second
 }
 delay(1000);
}

```